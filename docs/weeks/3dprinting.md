			**3D PRINTING**   
			
For 3D printing I use **tinkercad** which is a online website.   

website: https://www.tinkercad.com/   
   
![tinkercad](../img/3dprinting/printing2.png)   
    
Tinkercad is mostly used to create enclosures for components.   
    
People also download files from google and import it into tinkercad to make your enclosure faster and easier.   
     
(to check out how to import/export/use tinkercad check cad.md)   
     
     
     
If you would like to add a logo to your design follow the following steps:   
      
Use any online website to make your logo.   
For my logo I used this website: https://editor.freelogodesign.org/?lang=EN   
This was my logo:   
![logo](../img/3dprinting/printing3.png)   
    
      
	  

After downloading your created logo, use Inkscape to make your logo dark or light, then import it.    
Inkscape download: https://inkscape.org/release/inkscape-1.0.1/   
    
      
Head back to tinkercad, import the files, and you will be able to put your logo on your enclosure.    
    
    
      
    
After I export my file I open up cura which I use to fix my perimeters.     

Cura download: https://ultimaker.com/software/ultimaker-cura   
![cura](../img/3dprinting/printing4.png)   
      
After putting in my perimeters I export the file as Gcode and put it on a card.        
         
Put the card in a printer. Since everyone uses a different printer, I would advice to use the instruction book.   
         
You will now be able to print your enclosure. After the item is done printing, wait 10 mins to let it cool.   
       
My enclosure looks like this:   
        
![enclosure](../img/3dprinting/printing1.png)   