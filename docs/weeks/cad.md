To create a 3D object, I use tinkercad.    
Tinkercad link: https://www.tinkercad.com/       

If you would like to import a certain file just click on "import".   

You have to go to 3D objects and click "create new design"   

![newdesign](../img/cad/cad1.png)   


You take a object you want to work with (for starters the cube is the best)    
Drag the cube to the working surface.    

![box](../img/cad/cad2.png)   


To make the cube taller or lower you use the red button on the top.    

![high/low](../img/cad/cad4.png)   


To change height location you use the arrow on the top of the object.    

![height](../img/cad/cad3.png)    


To change thickness use any of these white buttons in the side.   
![thickness](../img/cad/cad16.png)   


If you would like to add a hole in your object, take one of these transparent boxes.    
Drag it to the surface.   
![invisible](../img/cad/cad5.png)      
  


To allign all objects together, click "allign" on the topright corner.      

![allign](../img/cad/cad7.png)   


To group everything into one object, select it all and click "group".      

![group](../img/cad/cad8.png)   


Now to export your work click "Export"
Then click "STL file"

![stlfile](../img/cad/cad11.png)   

          *FUSION 360*   

To start with Fusion 360 you go to 'insert' then click 'insert SVG'   	

![insert](../img/cad/cad12.png)   


Choose the file (SVG file) you are going to use   

![svg](../img/cad/cad13.png)   


Obviously the item (svg file) will not come in the form you want so we will make it higher    

![aidk](../img/cad/cad14.png)   


It should look something like this after you have made it taller   

![taller](../img/cad/cad15.png)   


Then export it..

![addon](../img/cad/cad17.jpg)   