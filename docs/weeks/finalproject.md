					**Smart Nature**
					
					

For my final project I decided to make Smart Nature.

Smart Nature is a IoT Agricultural project that automatically watches over your farm and has alot of advantages.

(For more detail check my google slides: https://docs.google.com/presentation/d/1QNmOjS040Usgdluypg4T1LKlH7PVbR7raTzoWGEEcJU/edit?usp=sharing  )



I first started off with coding my node sensor. I used LilyGO ESP32 (http://www.lilygo.cn/prod_view.aspx?TypeId=50033&Id=1126&FId=t3:50033:3 )
![esp32](../img/finalproject/fp1.jpg)
My node sensor (esp32) uses a USB C cable.

My second node was a DHT11 sensor which infact is my esp8266.

		**Coding.**
		

The software that I used to code my Node Sensor is Arduino IDE (https://www.arduino.cc/en/software)
![arduino](../img/finalproject/fp2.jpg)


I used two nodes. The esp32 and eps8266. Their codes are below:
esp8266 code:
**/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-esp-now-wi-fi-web-server/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/
#include <ESP8266WiFi.h>
#include <espnow.h>
//#include <esp_now.h>
//#include <WiFi.h>
#include "ESPAsyncWebServer.h"
#include <Arduino_JSON.h>

// Replace with your network credentials (STATION)
const char* ssid =  "Team09"; //"Virus";   //"Team09";
const char* password = "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message {
  int id;
  float temp;
  float hum;
  float soil;
  float salt;
  float light;
  float batt;
  float pump1;
  float pump2;
  unsigned int readingId;
} struct_message;

struct_message incomingReadings;

JSONVar board;

AsyncWebServer server(80);
AsyncEventSource events("/events");

//void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {
//  memcpy(&myData, incomingData, sizeof(myData));
//
//}
// callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac_addr, uint8_t *incomingData,  uint8_t len) { 
  //memcpy(&myData, incomingData, sizeof(myData));
  // Copies the sender mac address to a string
  char macStr[18];
  Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.println(macStr);
  memcpy(&incomingReadings, incomingData, sizeof(incomingReadings));
  
  board["id"] = incomingReadings.id;
  board["temperature"] = incomingReadings.temp;
  board["humidity"] = incomingReadings.hum;
  board["soil"] = incomingReadings.soil;
  board["salt"] = incomingReadings.salt;
  board["light"] = incomingReadings.light;
  board["batt"] = incomingReadings.batt;
  board["pump1"] =  digitalRead(14);
  board["pump2"] =  digitalRead(14);
  board["readingId"] = String(incomingReadings.readingId);
  String jsonString = JSON.stringify(board);
  events.send(jsonString.c_str(), "new_readings", millis());
  
  Serial.printf("Board ID %u: %u bytes\n", incomingReadings.id, len);
  Serial.printf("t value: %4.2f \n", incomingReadings.temp);
  Serial.printf("h value: %4.2f \n", incomingReadings.hum);
  Serial.printf("s value: %4.2f \n", incomingReadings.soil);
  Serial.printf("sa value: %4.2f \n", incomingReadings.salt);
  Serial.printf("l value: %4.2f \n", incomingReadings.light);
  Serial.printf("b value: %4.2f \n", incomingReadings.batt);
  Serial.printf("p1 value: %4.2f \n", incomingReadings.pump1);
  Serial.printf("p2 value: %4.2f \n", incomingReadings.pump2);
  Serial.printf("readingID value: %d \n", incomingReadings.readingId);
  Serial.println();

  handleIncoming();
}

void handleIncoming(){
// Handel Tap 

if (incomingReadings.id == 1) 
{
   // Executes when condition1 is true
   if ( incomingReadings.soil < 18) 
   {
    digitalWrite(14, HIGH); // sets the digital pin 13 on
    Serial.println("14 high"); 
   }else{
    digitalWrite(14, LOW);  // sets the digital pin 12 off
    Serial.println("14 low");
    }
}

if (incomingReadings.id == 2) 
{
   // Executes when condition1 is true
   if ( incomingReadings.soil < 18) 
   {
    digitalWrite(15, HIGH); // sets the digital pin 13 on
    Serial.print("15 high"); 
   }else{
    digitalWrite(15, LOW);  // sets the digital pin 12 off
    Serial.print("15 low");
    }
}
}
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Farm Buddies</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="icon" href="data:,">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    p {  font-size: 1.2rem;}
    body {  margin: 0;}
    .topnav { overflow: hidden; background-color: #088723; color: white; font-size: 1.7rem; }
    .content { padding: 20px; }
    .card { background-color: white; box-shadow: 2px 2px 12px 1px rgba(140,140,140,.5); }
    .cards { max-width: 700px; margin: 0 auto; display: grid; grid-gap: 2rem; grid-template-columns: repeat(auto-fit, minmax(200px, 1fr)); }
    .reading { font-size: 2.8rem; }
    .packet { color: #bebebe; }
    .card.temperature { color: #d55e52; }
    .card.humidity { color: #2e9bc5; }
    .card.soil { color: #cf8223; }
    .card.salt { color: #4665ac; }
    .card.light { color: #9346ac;}
    .card.batt { color: #e1dc2e; }
  </style>
</head>

  <div class="topnav">
   <div style= "text-align: left"> <a href="https://ibb.co/XLCDkSW"><img src="https://i.ibb.co/W520pgH/Whats-App-Image-2021-03-25-at-7-51-40-PM.jpg" alt="Whats-App-Image-2021-03-25-at-7-51-40-PM" border="0" height="100px" width="100px"></a></div>
  </div>
  <div class="content">
    <div class="cards">
      <div class="card temperature">
        <h4><i class="fas fa-thermometer-half"></i> Buddy 1 - TEMPERATURE</h4><p><span class="reading"><span id="t1"></span> &deg;C</span></p><p class="packet">Reading ID: <span id="rt1"></span></p>
      </div>
      <div class="card humidity">
        <h4><i class="fas fa-tint"></i> Buddy 1 - HUMIDITY</h4><p><span class="reading"><span id="h1"></span> </span></p><p class="packet">Reading ID: <span id="rh1"></span></p>
      </div>
      <div class="card soil">
        <h4><i class="fas fa-tint"></i> Buddy 1 - SOIL</h4><p><span class="reading"><span id="s1"></span> </span></p><p class="packet">Reading ID: <span id="rs1"></span></p>
      </div>
      <div class="card salt">
        <h4><i class="fas fa-tint"></i> Buddy 1 - SALT</h4><p><span class="reading"><span id="sa1"></span> </span></p><p class="packet">Reading ID: <span id="rsa1"></span></p>
      </div>
      <div class="card light">
        <h4><i class="fas fa-tint"></i> Buddy 1 - LIGHT</h4><p><span class="reading"><span id="l1"></span> </span></p><p class="packet">Reading ID: <span id="rl1"></span></p>
      </div>
      <div class="card batt">
        <h4><i class="fas fa-tint"></i> Buddy 1 - BATT</h4><p><span class="reading"><span id="b1"></span> &percnt;</span></p><p class="packet">Reading ID: <span id="rb1"></span></p>
      </div>
      <div class="card temperature">
//        <h4><i class="fas fa-thermometer-half"></i> Buddy 2 - TEMPERATURE</h4><p><span class="reading"><span id="t2"></span> &deg;C</span></p><p class="packet">Reading ID: <span id="rt2"></span></p>
//      </div>
//      <div class="card humidity">
//        <h4><i class="fas fa-tint"></i> Buddy 2 - HUMIDITY</h4><p><span class="reading"><span id="h2"></span> </span></p><p class="packet">Reading ID: <span id="rh2"></span></p>
//      </div>
//       <div class="card soil">
//        <h4><i class="fas fa-tint"></i> Buddy 2 - SOIL</h4><p><span class="reading"><span id="s2"></span> </span></p><p class="packet">Reading ID: <span id="rs2"></span></p>
//      </div>
//      <div class="card salt">
//        <h4><i class="fas fa-tint"></i> Buddy 2 - SALT</h4><p><span class="reading"><span id="sa2"></span> </span></p><p class="packet">Reading ID: <span id="rsa2"></span></p>
//      </div>
//      <div class="card light">
//        <h4><i class="fas fa-tint"></i> Buddy 2 - LIGHT</h4><p><span class="reading"><span id="l2"></span> </span></p><p class="packet">Reading ID: <span id="r12"></span></p>
//      </div>
//      <div class="card batt">
//        <h4><i class="fas fa-tint"></i> Buddy 2 - BATT</h4><p><span class="reading"><span id="b2"></span> &percnt;</span></p><p class="packet">Reading ID: <span id="rb2"></span></p>
//      </div>
//      <div class="pump">
//        <h4><i class="fas fa-tint"></i> Pump  - Pump1</h4><p><span class="reading"><span id="p1"></span> </span></p><p class="packet">Reading ID: <span id="rp1"></span></p>
//      </div>
//      <div class="pump">
//        <h4><i class="fas fa-tint"></i> Pump  - Pump2</h4><p><span class="reading"><span id="p2"></span> </span></p><p class="packet">Reading ID: <span id="rp2"></span></p>
//      </div>
    </div>
  </div>
<script>
if (!!window.EventSource) {
 var source = new EventSource('/events');
 
 source.addEventListener('open', function(e) {
  console.log("Events Connected");
 }, false);
 source.addEventListener('error', function(e) {
  if (e.target.readyState != EventSource.OPEN) {
    console.log("Events Disconnected");
  }
 }, false);
 
 source.addEventListener('message', function(e) {
  console.log("message", e.data);
 }, false);
 
 source.addEventListener('new_readings', function(e) {
  console.log("new_readings", e.data);
  var obj = JSON.parse(e.data);
  document.getElementById("t"+obj.id).innerHTML = obj.temperature.toFixed(2);
  document.getElementById("h"+obj.id).innerHTML = obj.humidity.toFixed(2);
  document.getElementById("s"+obj.id).innerHTML = obj.soil.toFixed(2);
  document.getElementById("sa"+obj.id).innerHTML = obj.salt.toFixed(2);
  document.getElementById("l"+obj.id).innerHTML = obj.light.toFixed(2);
  document.getElementById("b"+obj.id).innerHTML = obj.batt.toFixed(2);
  document.getElementById("rt"+obj.id).innerHTML = obj.readingId;
  document.getElementById("rh"+obj.id).innerHTML = obj.readingId;
  document.getElementById("p1"+obj.id).innerHTML = obj.pump1;
  document.getElementById("p2"+obj.id).innerHTML = obj.pump2;

 }, false);
}
</script>
</body>
</html>)rawliteral";

void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  pinMode(15, OUTPUT); 
  pinMode(14, OUTPUT); 
  // Set the device as a Station and Soft Access Point simultaneously
  WiFi.mode(WIFI_AP_STA);
  
  // Set device as a Wi-Fi Station
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Setting as a Wi-Fi Station..");
  }
  Serial.print("Station IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Wi-Fi Channel: ");
  Serial.println(WiFi.channel());

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });
   
  events.onConnect([](AsyncEventSourceClient *client){
    if(client->lastId()){
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  server.begin();
}
 
void loop() {

  static unsigned long lastEventTime = millis();
  static const unsigned long EVENT_INTERVAL_MS = 5000;
  if ((millis() - lastEventTime) > EVENT_INTERVAL_MS) {
    events.send("ping",NULL,millis());
    lastEventTime = millis();
  }
}**

The esp32 code:
**/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-esp-now-wi-fi-web-server/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <Wire.h>
#include <BH1750.h>

// Set your Board ID (ESP32 Sender #1 = BOARD_ID 1, ESP32 Sender #2 = BOARD_ID 2, etc)
#define BOARD_ID 1
#define POWER_CTRL          4
#define BAT_ADC             33
#define SALT_PIN            34
#define SOIL_PIN            32
#define I2C_SDA             25
#define I2C_SCL             26
// Digital pin connected to the DHT sensor
#define DHTPIN 16  
#define BOARD_ID 1  //Change Id for each additional node

BH1750 lightMeter(0x23); //0x23

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
DHT dht(DHTPIN, DHTTYPE);

//MAC Address of the receiver 
uint8_t broadcastAddress[] = {0xBC, 0xDD, 0xC2, 0x10, 0x4B, 0x33};
//84:0D:8E:85:FE:1C
//BC:DD:C2:10:4B:33

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    int id;
    float temp;
    float hum;
    float soil;
    float salt;
    float light;
    float batt;
    int readingId;
} struct_message;

//Create a struct_message called myData
struct_message myData;

unsigned long previousMillis = 0;   // Stores last time temperature was published
const long interval = 10000;        // Interval at which to publish sensor readings

unsigned int readingId = 0;

// Insert your SSID
constexpr char WIFI_SSID[] = "Team09" ; // "Virus"; //Team09;

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}

float readDHTTemperature() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //float t = dht.readTemperature(true);
  // Check if any reads failed and exit early (to try again).
  if (isnan(t)) {    
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  else {
    Serial.println(t);
    return t;
  }
}

float readDHTHumidity() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  if (isnan(h)) {
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  else {
    Serial.println(h);
    return h;
  }
}

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  //Init Serial Monitor
  Serial.begin(115200);

  dht.begin();
  pinMode(POWER_CTRL, OUTPUT);
  digitalWrite(POWER_CTRL, 1);  

  //lux 
  if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
        Serial.println(F("BH1750 Advanced begin"));
    } else {
        Serial.println(F("Error initialising BH1750"));
    }

  // Set device as a Wi-Fi Station and set channel
  WiFi.mode(WIFI_STA);

  int32_t channel = getWiFiChannel(WIFI_SSID);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}
// functions here 

uint16_t readSoil()
{
    uint16_t soil = analogRead(SOIL_PIN);
    return map(soil, 0, 4095, 100, 0);
}

uint32_t readSalt()
{
    uint8_t samples = 120;
    uint32_t humi = 0;
    uint16_t array[120];

    for (int i = 0; i < samples; i++) {
        array[i] = analogRead(SALT_PIN);
        delay(2);
    }
    std::sort(array, array + samples);
    for (int i = 0; i < samples; i++) {
        if (i == 0 || i == samples - 1)continue;
        humi += array[i];
    }
    humi /= samples - 2;
    return humi;
}

float readBattery()
{
    int vref = 1100;
    uint16_t volt = analogRead(BAT_ADC);
    float battery_voltage = ((float)volt/4095.0) * 100 ;
    //float battery_voltage = ((float)volt/4095.0) * 2.0 * 3.3 * (vref);
    return battery_voltage;
}
void loop() {
    digitalWrite(POWER_CTRL, 1); // Turn power on dht
    delay(1000);
    
    Serial.println("soil");
    uint16_t soil = readSoil();
    Serial.println(soil);
    
    Serial.println("salt");
    uint32_t salt = readSalt();
    Serial.println(salt);
    Serial.println("light");
   float light = lightMeter.readLightLevel();
    Serial.println(light);
    Serial.println("batt");
    float batt = readBattery();
    Serial.println(batt);
    
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Save the last time a new reading was published
    previousMillis = currentMillis;
    //Set values to send
    myData.id = BOARD_ID;
    myData.temp = readDHTTemperature();
    myData.hum = readDHTHumidity();
    myData.soil = readSoil();
    myData.salt = readSalt();
    myData.light = lightMeter.readLightLevel();
    myData.batt = readBattery();
    myData.readingId = readingId++;
     
    //Send message via ESP-NOW
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
      
    }
  }
    digitalWrite(POWER_CTRL, 1); // Turn power on dht
}**



			**Enclosure**
			

We are going to 3D print our enclosure. I found my enclosure online. (to check mine out https://www.cnx-software.com/2020/11/10/ttgo-t-higrow-is-a-wifi-bluetooth-connected-soil-temperature-moisture-sensor/  )



The website you can use to create your logo = https://editor.freelogodesign.org/?lang=EN

Once you downloaded your logo, to go Inkscape ( https://inkscape.org/ ) and stick with the following steps:

1. File - open (open the logo you just downloaded)
![logo](../img/finalproject/fp7.jpg)


2. Make sure you SELECT your logo and click Path - Trace Bitmap.
Have live preview on all the time, and play around with the threshold until you get your logo fully and neatly in black and white.    
![tracebitmap](../img/finalproject/fp8.jpg)


3. Once completed with the first two steps, you are done in Inkscape. Save the file now..  File - Save as..
![Inkscape](../img/finalproject/fp9.jpg)


4. Head to Tinkercad ( https://www.tinkercad.com ), and import the logo (SVG file).
![import](../img/finalproject/fp10.jpg)

5. Put the logo into your design. Make sure you change it to a hole. Select it and group it with the part it is in.
![group](../img/finalproject/fp11.jpg)

My design:
![mydesign](../img/finalproject/fp5.jpg)


6. Export the SVG file and import it into Cura. ( https://ultimaker.com/software/ultimaker-cura )

7. Change the perimeters and put it on a SD card which you shall insert into your 3D printer. Start printing your design.

My printed design:
![finished3dprinting](../img/finalproject/fp6.jpg)






			**creating my demo**


For my demo, I decided laser cutting would be a good idea.

I started off with downloading a box which you could put into each other. ( https://en.makercase.com/#/ ) There were several shapes to choose from: Bent boxes, basic boxes, and even Polygon boxes.
I liked the Polygon box so I chose that one. Make sure to have the perimeters in millimeters instead of inches.
![mm](../img/finalproject/fp12.jpg)




For my height I had 150mm but it really depends on how big you want your box. After choosing my perimeters, scroll down and click "download"
Circled in red is what you need to click. The rest is pretty much optional. Make sure you do download a DXF file and not SVG.
![downloadsvg](../img/finalproject/fp13.jpg)






After finishing this part, open/download Librecad. ( https://librecad.org/ )
Since all the 6 parts are together in 1 file, we will seperate them into seperate files.
Stick by the following steps which will guide you through the process of Librecad:





1. File - Open (select the file that you downloaded as DXF in MakerCam)
![makercam](../img/finalproject/fp14.jpg)

2. Go to Options - Drawing Preferences
![drawingpref](../img/finalproject/fp15.jpg)

Make sure these are your selected options:
![options](../img/finalproject/fp16.jpg)


3. Discover which part is in which layer. (use the eye to make layers visible/invisible to see that)
   If you are going to work on object 3, make sure you are in the layer of object 3 so it stays seperated from the rest.
![layers](../img/finalproject/fp17.jpg)


4. Now to seperate them, select ONE of the objects, and explode it.
![explode](../img/finalproject/fp18.jpg)
![explodeoptions](../img/finalproject/fp18.jpg)

5. Now you have all the lines seperated. One part has around 10-20 seperated lines which we do not want. One part must be complete. To fix this, select one part again.
![choosee](../img/finalproject/fp23.jpg)

Click "Create Polyline from existing segments"   
![polyline](../img/finalproject/fp19.jpg)
They are combined and one part complete now.   




6. Select ONE of the parts now (the one you already worked on) and go to File - Export - Export as Makercam SVG
![invisiblelayers](../img/finalproject/fp20.jpg)
![exportsvg](../img/finalproject/fp21.jpg)


7. Make sure these are your selected options:
![chosenoptions](../img/finalproject/fp22.jpg)

8. Repeat this process with all 6 parts (or however much you have).

Import the files in Tinkercad to see how it would come out.
![tinkercadimport](../img/finalproject/fp24.png)

I also added a Logo to my design in Tinkercad. If you'd like to add a logo as well, the steps are in the "enclosure" section.


For measuring your parameters in Librecad, use this tutorial: https://www.youtube.com/watch?v=6Hp6LdU6TFM

							**Camming and CNC cutting** 
							(computer-aided manufacturing and computer numerical control)

We will use Aspire (= https://www.aspire.com/ )

Material thickness = 12.5mm
Height = 150mm
width = 130mm
Feedrate = 1200
Plunge = 600
Stepdown = 1/2
Bit = 3mm

We were first going to do a test cut, and we used these perameters for that:
![testingg](../img/finalproject/fp26.jpg)
![testing2](../img/finalproject/fp25.jpg)

The material I used: ![material](../img/finalproject/fp27.jpg)

Time to CNC cut

![cutting](../img/finalproject/fp28.jpg) 

![cuttingg](../img/finalproject/fp29.jpg) 

![cuttinggg](../img/finalproject/fp30.jpg)

-

**Getting the project to work**

First, you power up the esp32 and esp8266 (plug it in your computer or wherever after uploading the codes). The esp32 senses information (water, salt, temperature humidity, light intensity, soil).
The esp32 will send that information to the esp8266. The esp8266 has the dashboard. Once it received the information from the esp32, it puts it on the dashboard.

To let the esp32 send its information to the esp8266 you need to put the esp8266 MAC adress in the esp32 code.
A good example is for example phones. You cannot call another phone to deliver information without their number.
Now the question is, how do you get the esp8266 MAC adress? Well its simple. Enter the following code in arduino uno and upload it to your esp8266.


**#include <ESP8266WiFi.h>

void setup(){
  Serial.begin(115200);
  Serial.println();
  Serial.print("ESP8266 Board MAC Address:  ");
  Serial.println(WiFi.macAddress());
}
 
void loop(){

}

// MAC ADRESS FOR ESP32 

//#include "WiFi.h"
//void setup(){
//  Serial.begin(115200);
//  WiFi.mode(WIFI_MODE_STA);
//  Serial.println(WiFi.macAddress());
//}
//void loop(){
//}**

Open the serial monitor on arduino uno after uploading this code. There will be a MAC adress displayed here.
Upload the esp8266 code back now. In the esp32 code, add the esp8266's MAC address. The 2 nodes will now be able to communicate with eachother.


If you'd like to check if the eps32's information is being send to the esp8266, open the 8266's serial monitor. You will see the esp32 info there.

![dasboard](../img/finalproject/fp34.png)


This was the result. Added a plant to make it look nice
![result](../img/finalproject/fp31.jpg)

Working project:
![done](../img/finalproject/fp32.jpg)
![donee](../img/finalproject/fp33.jpg)

This brings about an end to my final project. Thank you for reading! :D 
