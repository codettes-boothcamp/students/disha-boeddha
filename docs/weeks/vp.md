You open Adobe Premiere   

![adobe](../img/disha1.png)    


Go to *file*,*new*,*project*   

![start](../img/disha2.png)    


Write any name you would like and click *OK*    

![clickok](../img/disha3.png)   


Go to the file or video you would like to edit   

![goto](../img/disha4.png)   


Drag it to the left down corner to edit   

![leftdown](../img/disha5.png)   


Now drag it to the right corner(from left to right) (also called the timeline)   

![timeline](../img/disha6.png)   


To import your video, go to *file*, and then *import*   

![import](../img/disha7.png)   
 

To add effects, click on the two arrows in the left down corner. It will give options. Click on *effects*   

![effects](../img/disha8.png)   


After that search for *ultra key*   

![ultrakey](../img/disha9.png)   


Drag it to the video   

![dragtovid](../img/disha10.png)   


Choose a picture you would like to add as a background   

![choosepls](../img/disha11.png)   


Drag it to the middle of the timeline   

![middleeeee](../img/disha12.png)   


For adding a title click on *file*,*new*,*Legacy title*   

![legtitle](../img/disha13.png)   


Add the title and color it (if you want to)   

![hurryapppp](../img/disha14.png)   


Add it at the bottom of the timeline just like you did with the background   

![bottomofline](../img/disha15.png)   


Saving the video go to *file*,*export*,*Media*   

![export](../img/disha16.png)   


Let the format be *H.264*    

![format](../img/disha17.png)   


And let the one under it be *Youtube 720HP*   

![yt](../img/disha18.png)   