Install Kicad    https://kicad-pcb.org/download    

Install Flatcam   http://flatcam.org/download    



We will start opening Kicad and follow what I do      

Go to file-new-project   

![new](../img/ed/ed1.png)   



Go to this button   

![button](../img/ed/ed2.png)    



Go to preferences-manage symbol libraries   

![pref](../img/ed/ed3.png)   



Create this:    

![create](../img/ed/ed4.png)   



Append it    

![append](../img/ed/ed5.png)     



Find the components and footprints   



Generate netlist    
(in the top right corner)    

![netlist](../img/ed/ed6.png)   



It will now be a black version    

![oops](../img/ed/ed7.png)   


Use the green button for wiring    

![wiring](../img/ed/ed8.png)    



Connect the components with the wires    

![connect](../img/ed/ed9.png)    



Go to file-plot    

![plot](../img/ed/ed10.png)    




         ***FLATCAM***
		 
		 

Go to file-open gerber    

![gerber](../img/ed/ed11.png)    



Front cuts    
Cut Z  -0.12    
 
Travel Z 2.0     

Feedrate  30mm/min    
 
Tool dia 0.2    

Spindle-speed 1000     



Edge cuts:       
Cut Z  -1.5       

Travel Z 0.1      

Feedrate  20 mm/min        

Tool dia 0.8       

Spindle-speed 1000         

Multi Depth (check)       

Depth pass 0.5      




Export it     

 
 Cutout:    
 Cut Z  -1.5     
  
Travel Z 0.1       

Feedrate  20 mm/min      

Tool dia 0.8      

Spindle-speed 1000      

Multi Depth (check)       

Depth pass 0.5       

Tool dia 0.8      

Spindle-speed 6000  max 1000     

Multi depth (check)      

Depth-pass 0.5       



