## Project Managment

here you clone the link

![clone](../img/pic1.png)


here you do the desktop clone

![desktopclone](../img/pic2.png)


here you go to the local file system

![localfilesystem](../img/pic3.png)


here you make the change in notepad++

![notepad](../img/pic4.png)


It will change automatically

![autochange](../img/pic5.png)


You click on commit change

![commit](../img/pic6.png)


You can now push it towards the cloud

![push](../img/pic7.png)


The gitlab change is now made

![change](../img/pic8.png)