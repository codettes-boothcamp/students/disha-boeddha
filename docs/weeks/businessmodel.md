To make your own business model canvas you can use this link:    
https://canvanizer.com/new/business-model-canvas    
    
If you do not know what to fill in you may use my canvas as reference, but I will give a short description.   
    
**costumer segment** - The people or companies that can make use of my product.    
**Value preposition** - What the product includes that can help people with.    
**Channels** - How I will promote my product (for example what kind of social platforms).   
**Problems** - Why I created this certain product that will help people.    
**Revenue Streams** - The income generated.   
**Solution(s)** - How this project helps people. (for example winning time, more profit etc.)   
**Brainstorming Space** - To find a conclusion for a specific problem by gathering a list of ideas.    
     
	 
	 
This is my business model canvas:   
    
     
![canvas](../img/businessmodelcanvas/finalcanvas.jpg)   