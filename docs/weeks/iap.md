This is what a raspberry Pi looks like:    

![oo](../img/iap/iap14.png)    


Open Rasperry Pi   

![pi](../img/iap/iap1.jpg)  


Open Commandment promt and find ur IP adress   


Insert the SD card into your laptop      

Write Raspian Jessie on your card using Win32DiskImager     


To take a shortcut,   
add a **folder** on the raspberry named  *ssh**    


On the SD card there is a **file** named cmdline.txt   
Edit it with notepadd++ (if u don't have it, here is the link to download it https://notepad-plus-plus.org/downloads/ )    


Write your IP adress into that file, save it, and put it now into raspberry PI    


Open putty    
(if you don't have it download it here  https://www.putty.org/ )     

Host name: **(ur IP adress)**     

![puttyopen](../img/iap/iap15.png)    



After this it will ask you for a     
username: **raspberry**    
password: **pi**     


Once your logged in, type in  sudo raspi-config (this will give you acces to SSH and more)     


            **Nodejs and PYthon**    
			
			**controls**    
cd= To enter a folder      
cd ..= to leave a folder     
ls= to show what is inside a folder  
touch= to make a FILE   
mkdir= to create a folder   



The nodejs folder structure:    

nodejs      
projects     
project1   
index.js    
public   
index.html    
css    
style.css     
package.json      



Install python by typing these:    

apt-get update     
apt-get upgrade      
apt-get install python      
apt-get install python-pip     



Install nodejs by typing these:    

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs    
 node --version     
 npm --version     
 
 
 
 In project1, create a file named index.js    
 
 
Install **Nodemon** which will update nodejs when changes are made.    

 npm i -D nodemon   
 
 
 
 To use the express command line, install express by typing in putty:    
 npm i express    
 
 
 delete the test script in package.json and create a new dev    
 
the code:    
'''
 {      
      "name": "whatabyte-portal",    
      "version": "1.0.0",    
      "description": "",     
      "main": "index.js",     
      "scripts": {
        "dev": "nodemon ./index.js"     
      },
      "keywords": [],     
      "author": "",     
      "license": "ISC",     
      "devDependencies": {      
        "nodemon": "^1.19.3"      
      }         
    }         
'''

Put this in index.js    

 // index.js     
 
    /**     
     * Required External Modules     
     */        
  
    /**     
     * App Variables      
     */     

    /**      
     *  App Configuration      
     */       

    /**     
     * Routes Definitions      
     */       

    /**
     * Server Activation      
     */      
	 
	 
	 code for required external modules:    
	 
	  const express = require("express");     
    const path = require("path");    
	

	
	For  App Variables:    
	
const app = express();     
    const port = process.env.PORT ||"8000";     

	
for   App Configuration      

  app.use(express.static(path.join(__dirname, "public")));     
  
  
Routes and definition= empty    


For server activation:    

 app.listen(port,() => {      
      console.log(`Listening to requests on http://localhost:${port}`);     
    });    
	
	

Go to public folder and add the     

**basic HTML**   
<html>    
    <body>    
    <h1>My website</h1>     
    </body>     
    </html>    
	
 
Type in npm run dev    

Type in browser your Ip adress    

You will now see "my website"   

![mYY](../img/iap/iap16.png)    

To decorate your website you use HTML,Javascript,CSS    






           we will now setup a server using Flask, Phyton and HTML/CSS.     
		   
Pyton folder structure:    

python    

Index.py    
static     

img     
templates     

index.html    


Put this code in index.py    

  from flask import Flask, render_template    

    app = Flask(__name__)      

    @app.route('/')       
    def index():       
        return render_template('index.html')        
    if __name__ == '__main__':      
        app.run(debug=True, host='0.0.0.0')      
		
type the command  python index.py    


Put the basic HTML inside index.html    





            **SERIAL COMMUNICATION**    
			
Open Arduino (if u don't have it install here https://www.arduino.cc/en/main/software )    

Type  lsusb to see what ur arduino uno is connected to   

Add this code to make the led blink:   

'''
  import serial
    import time
    # Define the serial port and baud rate.
    # Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager

    ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
    def led_on_off():
        user_input = input("\n Type on / off / quit : ")
        if user_input =="on":
            print("LED is on...")
            time.sleep(0.1) 
            ser.write(b'H') 
            led_on_off()
        elif user_input =="off":
            print("LED is off...")
            time.sleep(0.1)
            ser.write(b'L')
            led_on_off()
        elif user_input =="quit" or user_input == "q":
            print("Program Exiting")
            time.sleep(0.1)
            ser.write(b'L')
            ser.close()
        else:
            print("Invalid input. Type on / off / quit.")
            led_on_off()

    time.sleep(2) # wait for the serial connection to initialize

    led_on_off()
'''
	
	
To make it work type: sudo python    


Type on to make the led go on   
Type off to make it go off   







                    **HTML AND CSS**
					
					
Root folder=public folder    

index.html
img

css

fonts

js

lib   


The code:   

'''
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Title</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name ="viewport" content ="width=device-width, initial-scale=1.0">
</head>

<body>
    <header class ="mainHeader">
        <img src="img/logo.png">
    <nav>
          <ul>
                <li><a href="#" class="active">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Contact</a></li>
        </ul>
        </nav>
    </header>

    <div class="mainContent">
        <div class="content">
            <article class="articleContent">
                <header>
                    <h2>First Article #1</h2>
                </header>

                <footer>
                    <p class="post-info">Written by Super Woman</p>                 
                </footer>
                <content>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </content>
            </article> 

            <article class="articleContent">
                <header>
                    <h2>2nd Article #2</h2>
                </header>

                <footer>
                    <p class="post-info">Written by Super Woman</p>                 
                </footer>

                <content>
                    <p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
                </content>
            </article> 

       </div>
    </div>

    <aside class="top-sidebar">
        <article>
            <h2>Top sidebar</h2>
            <p>Lorum ipsum dolorum on top</p>
        </article>
    </aside>

    <aside class="middle-sidebar">
        <article>
            <h2>Middle sidebar</h2>
            <p>Lorum ipsum doloru in the middle</p>
        </article>
    </aside>

    <aside class="bottom-sidebar">
        <article>
            <h2>Bottom sidebar</h2>
            <p>Lorum ipsum dolorum at the bottom</p>
        </article>
    </aside>

    <footer class="mainFooter">
        <p>Copyright &copy; <a href="#" title = " MyDesign">mywebsite.com</a></p>
    </footer>

</body>

</html>
'''


Check your page now    


The code to put colors:    

<link href="css/style.css" rel="stylesheet" />    


Code for resulting CSS code:    

'''
/* 
    Stylesheet for:
    HTML5-CSS3 Responsive page
*/

/* body default styling */

body {
    /* border: 5px solid red; */
    background-image: url(img/bg.png);
    background-color: #000305;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}


/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #8B008B;
    color: #FFF;
}


/* define mainHeader image and navigation */
.mainHeader img {
    width: 30%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
}
''' 
 
 
![article}(../img/iap/iap18.png)    

Article 2 is added in the body    
Javascript at the bottom (above the body)    



Objects and arrays:    


Object={
        }

And 

Array = [ , , ]

GetElementByTag

= wordt
== equal to
+= was and new

examples:
Array: 
var mygifs[ ];
mygifs=["car","boat","bff"]
if we choose to have "bff" then we write: mygifs(2);

mygifs.shift (adds a value)

We array: 
1. shift
2. push
3. pop

Object:
'''
var myObj{};
value pairs
    ={temp:"10",
        highTemp:[, ,],
        onclick:","
    }
'''	
	
type this in your nodejs folder   

sudo npm install --save mqtt   


We replace index.js with this:    
'''
var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});
'''


Socket IO:    

'''
io.sockets.on('connection', function (socket) {

  socket.on('subscribe', function (data) {
    console.log('Subscribing to '+data.topic);
    socket.join(data.topic);
    client.subscribe(data.topic);
  });
  socket.on('control', function (data) {
    console.log('Receiving for MQTT '+ data.topic + data.payload);
    // TODO sanity check .. is it valid topic ... check ifchannel is "mqtt" and pass message to MQTT server
    client.publish(data.topic, data.payload);
  }); 
});
'''



Build the leds connected to Rpi   

Add scada project to a new project folder   

Type these:
				**npm install express**     
				**npm install socket.io**     
				**sudo apt-get install pigpio**     
				**npm install pigpio**     
				**sudo node app.js**   


App.js must have this code:    

'''
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
    // Hookup button behaviour within socket to submit an update when pressed (TO DO)

    // when the client emits 'opencmd', this listens and executes
    socket.on('opencmd', function (data) {
        // Add calls to IO here
        io.sockets.emit('opencmd', socket.username, data);
        setTimeout(function () {
            io.sockets.emit('openvalve', socket.username, data);
            console.log("user: "+socket.username+" opens LED" + data);
            // add some error handling here
            led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
            led.digitalWrite(1);
        }, 1000)
    });

    // when the client emits 'closecmd', this listens and executes
    socket.on('closecmd', function (data) {
        // Add calls to IO here
        io.sockets.emit('closecmd', socket.username, data);
        setTimeout(function () {
            io.sockets.emit('closevalve', socket.username, data);
            console.log("user: "+socket.username+" closes LED" + data);
            // add error handling here
            led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
            led.digitalWrite(0);
        }, 1000)

/*      setTimeout(function () {
            led.digitalWrite(0);
            io.sockets.emit('closevalve', socket.username, data);
        }, 1000) */
    });

});
'''


Index.html must have this code    


'''
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<meta http-equiv="X-UA-Compatible" content="IE=9" />

<head>
    <title>Codettes Tutorial</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link type="text/css" rel="stylesheet" href="css/app.css" media="all">
</head>
<script src="js/socket.js"></script>
<script src="js/jquery.js"></script>
<script>
    var socket = io.connect(window.location.hostname + ":4000"); //'http://192.168.1.163:4000'); //set this to the ip address of your node.js server

    // listener, whenever the server emits 'openvalve', this updates the username list
    socket.on('opencmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('red').addClass('green');
    });
    socket.on('openvalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('red').addClass('green');
    });


    // listener, whenever the server emits 'openvalve', this updates the username list
     socket.on('closecmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('green').addClass('red');
    });
    socket.on('closevalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('green').addClass('red');
    });

    // on load of page
    $(function() {

        // when the client clicks OPEN
        $('.open').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked open : " + id);
            socket.emit('opencmd', id);
        });

        // when the client clicks CLOSE
        $('.close').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked on close : " + id);
            socket.emit('closecmd', id);
        });

    });
</script>

<body>
    <p>

        <div id="17" class="valve">
            <h3>Control 1</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="27" class="valve">
            <h3>Control 2</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="18" class="valve">
            <h3>Control 3</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="23" class="valve">
            <h3>Control 4</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
</body>

'''

You will now be able to control your leds through browser   

![browser](../img/iap/iap19.png)    


				