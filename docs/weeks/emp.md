My phone was glitching and I couldn't make pictures to trasnfer from phone to computer...    




Install Arduino       

![Arduino](../img/emp/Arduino1.png)      


To get the device you will work on go to *tools,Board* and choose your device.    

![irjg](../img/emp/arduino2.png)   


Right now we will work on the *BLINK* option.     
To get that, click on *FILE*,*examples*,*0.1 Basics, Blink*      

![idk](../img/emp/arduino3.png)    


This is the code you use to Blink your LED lights   

![code](../img/emp/arduino4.png)   


This is the code for the spaceship interface.    

![spaceship](../img/emp/arduino5.png)       


Use the small arrow on the top tight corner to upload a code to your Arduino Uno (or whatever device u use)    

![arrow](../img/emp/arduino6.png)          


For running Leds use this code                     

![leds](../img/emp/arduino7.png)          


Serial Communication is the connection between your board and your Integrated Drive Electronics (IDE) .      

![serial](../img/emp/arduino8.png)            


The LDR (light dependent resistor) changes the resistor depending on the amount of light that the surrounding has.              

![LDR](../img/emp/arduino9.png)                     


To put the analog output (PWM) you use this code              

![analog](../img/emp/arduino10.png)           


PWM 1.2           

![pwm](../img/emp/arduino11.png)           


The code when you add a second LED        

![second](../img/emp/arduino12.png)      


To control the speed, you use this
![TSB](../img/emp/arduino13.png)       
and it's code is;  

   
'''// Declare ur variables     
const int pwm = 3;      
const int in_1 = 8;     
const int in_2 = 9;      


void setup(){     
	pinMode(in_1, OUTPUT);	     
  	pinMode(in_2, OUTPUT);         
  	pinMode(3, OUTPUT);    
  	Serial.begin(9600);     
}
void loop(){     
  	int duty = (analogRead(A0)-512)/2;      
  	Serial.println(duty);       
  	analogWrite(pwm,abs(duty));      
  	if(duty>0){      
        // turn CW    
      digitalWrite(in_1,LOW);     
      digitalWrite(in_2,HIGH);        
    }  
  	if(duty<0){     
        // turn CCW    
      digitalWrite(in_1,HIGH);    
      digitalWrite(in_2,LOW);      
    }   
  	if(duty==0){            
        // BRAKE              
      digitalWrite(in_1,HIGH);               
      digitalWrite(in_2,HIGH);         
    }      
}    



The Hbridge switches voltage.        

![voltage](../img/emp/arduino14.png)        
const int pwm = 3 ; //initializing pin 2 as pwm       
const int in_1 = 8 ;         
const int in_2 = 9 ;       
//For providing logic to L298 IC to choose the direction of the DC motor        

void setup() {           
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output     
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output      
   pinMode(in_2,OUTPUT) ;          
}

void loop() {           
   //For Clock wise motion , in_1 = High , in_2 = Low          
   digitalWrite(in_1,HIGH) ;                
   digitalWrite(in_2,LOW) ;             
   analogWrite(pwm,250) ;               
   /* setting pwm of the motor to 255 we can change the speed of rotation             
   by changing pwm input but we are only using arduino so we are using highest             
   value to driver the motor */         
   //Clockwise for 3 secs          
  delay(3000) ;         
   //For brake      
  digitalWrite(in_1,HIGH) ;      
  digitalWrite(in_2,HIGH) ;     
  delay(1000) ;     
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH   
  digitalWrite(in_1,LOW) ;   
  digitalWrite(in_2,HIGH) ;   
  delay(3000) ;   
   //For brake    
  digitalWrite(in_1,HIGH) ;   
  digitalWrite(in_2,HIGH) ;    
  delay(1000) ;      
}
'''


Next is the Motor Control Servo.     

The purpose of the Motor Control Servo is that it receives a signal and apply power to the DC motor, and it will continue untill
it is in the right position.      

![mcs](../img/emp/arduino15.png)    

code: 
'''  
#include <Servo.h>    

Servo myServo;   

int const potPin= A0;   
int potVal;    
int angle;    

void setup (){   
  myServo.attach(5);   
  Serial.begin(9600);   
 
}

void loop(){   
  potVal= analogRead(potPin);   
  Serial.print("potVal:");   
  Serial.print(potVal);   
  angle= map(potVal,0,1023,0,179);   
  Serial.print(",angle:");   
  Serial.println(angle);   
  //analogWrite(5, map(potVal,0,1023,0,127));   
  myServo.write(angle);      
  delay(15);   
}
'''  