				Laser cutting, Milling, and 3D printing, Compute aided design
				
				
Gcode for laser cutting:   
![code](../img/cam/cam1.png)   


To export go to file-export-exportSVG   

 
go to openSCAD       
go to CAM-pocket operation     
![pocket](../img/cam/cam2.png)    


To check all your operations go to CAM-all toolpaths   
![check](../img/cam/cam3.png)    


										3D PRINTING    
										
Put the SD card in the printer and turn it on.   
![sd](../img/cam/cam4.png)    


Choose the file and start printing your object (give the printer a few mins to warm up btw)    


								MILLING, CUTTING    
								
A cutting board can cut wood, steel, and plastic    

